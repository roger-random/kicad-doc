:experimental:

== Inspecting a board

=== Measurement tool

The measurement tool allows you to make distance and angle measurements between points on the PCB.
To activate the tool, click the image:images/icons/measurement_24.png[] icon in the right toolbar,
or use the hotkey kbd:[Ctrl + Shift + M].  Once the tool is active, click once to set the
measurement start point, then click again to finish a measurement.

NOTE: The measurement tool is used for quick measurements that do not need to be displayed
      permanently.  Any measurement you make will only be shown while the tool is active.  To
      create permanent dimensions that will appear in printouts and plots, use the Dimension tools.

=== Design rule checking

The design rule checker is used to verify that the PCB meets all the requirements established in
the Board Setup dialog and that all pads are connected according to the netlist or schematic.
KiCad can automatically prevent some design rule violations while routing tracks, but many others
cannot be prevented automatically.  This means it is important to use the design rule checker
before creating manufacturing files for a PCB.

To use the design rule checker, click the image:images/icons/erc_24.png[] icon in the top toolbar,
or select **Design Rules Checker** from the **Inspect** menu.

image::images/drc_control.png[scaledwidth="70%"]

The top section of the DRC Control window contains some options that control the design rule
checker:

**Refill all zones before performing DRC:** when enabled, zones will be refilled every time the
design rule checker is run.  Disabling this option may result in incorrect DRC results if zones
have not been refilled manually.

**Report all errors for each track:** when enabled, all clearance errors will be reported for each
track segment.  When disabled, only the first error will be reported.  Enabling this option will
result in the design rule checker running more slowly.

**Test for parity between PCB and schematic:** when enabled, the design rule checker will test for
differences between the schematic and PCB in addition to testing the PCB design rules.  This option
has no effect when running the PCB editor in standalone mode.

After running DRC, any violations will be shown in the center part of the DRC Control window.
Rule violations, unconnected items, and differences between the schematic and the PCB are shown in
three different tabs.  The controls below the list of violations can be used to show or hide
violations depending on their severity.  A report file in plain text format can be created after
running DRC using the Save button.

image::images/drc_violations.png[scaledwidth="70%"]

Each violation involves one or more objects on the PCB.  In the list of violations, the objects
involved are listed below the violation.  Clicking on the violation in the list view will move
the PCB Editor view so that the affected area is centered.  Clicking on one of the objects involved
in a violation will highlight the object.

The numbers at the bottom of the window show the number of errors, warnings, and
exclusions. Each type of violation can be filtered from the list using the
respective checkboxes. Clicking **Delete Markers** will clear all violations
until DRC is run again.

Violations can be right-clicked in the dialog to ignore them or change their
severity:

* *Exclude this violation:* ignores this particular violation, but does not
  affect any other violations.
* *Change severity:* changes a type of violation from warning to error, or error
  to warning. This affects all violations of a given type.
* *Ignore all:* ignores all violations of a given type. This test will now
  appear in the **Ignored Tests** tab rather than the **Violations** tab.

Excluded and ignored violations are remembered between runs of the design rule
checker.

==== Clearance and constraint resolution

The clearance and constraint resolution tools allow you to inspect which clearance and design
constraint rules apply to selected items.  These tools can help when designing PCBs with complex
design rules where it is not always clear which rules apply to an object.

To inspect the clearance rules that apply between two objects, select both
objects and choose **Clearance Resolution** from the **Inspect** menu.  The
Clearance Report dialog will show the clearance required between the objects on
each copper layer, as well as the design rules that resulted in that clearance.

image::images/clearance_resolution.png[scaledwidth="70%"]

To inspect the design constraints that apply to an object, select it and choose **Constraints
Resolution** from the **Inspect** menu.  The Constraints Report dialog will show any constraints that
apply to the object.

image::images/constraints_resolution.png[scaledwidth="70%"]

[[board-statistics]]
=== Board Statistics

The Board Statistics dialog shows a summary of the board's contents, including the number of
components, pads and vias; each by their own types as well as the overall board size.

image::images/Pcbnew_board_statistics.png[scaledwidth="70%"]

=== Find tool

The Find tool searches for text in the PCB, including reference designators,
footprint fields, and graphic text. When the tool finds a match, the canvas is
zoomed and centered on the match and the text is highlighted. Launch the tool
using the (image:images/icons/find_24.png[Find icon]) button in the top toolbar.

image::images/find_dialog.png[alt="Find dialog",scaledwidth="50%"]

The Find tool has several options:

*Match case:* Selects whether the search is case-sensitive.

*Words:* When selected, the search will only match the search term with complete
words in the PCB. When unselected, the search will match if the search
term is part of a larger word in the PCB.

*Wildcards:* When selected, wildcards can be used in the search terms. `?`
matches any single character, and `\*` matches any number of characters. Note
that when this option is selected, partial matches are not returned: searching
for `abc*` will match the string `abcd`, but searching for `abc` will not.

*Wrap:* When selected, search results will return to the first hit after
reaching the last hit.

*Search footprint reference designators:* Selects whether the search should
apply to footprint reference designators.

*Search footprint values:* Selects whether the search should apply to footprint
value fields.

*Search other text items:* Selects whether the search should apply to other text
items, including graphical text and footprint fields other than value and
reference.

*Search DRC markers:* Selects whether the search should apply to the violation
descriptions of DRC markers shown on the board.

*Search net names:* Selects whether the search should apply to the names of nets
in the board.

=== Search panel

The search panel is a docked panel that lists information about footprints,
zones, nets, ratsnest lines (unrouted segments), and text from the PCB. You can
optionally filter the list based on a search string. When no filter is used, all
items in the design are listed in the corresponding tab.

image::images/search_panel.png[alt="Search panel, with a footprint selected",scaledwidth=80%]

Items are filtered based on their properties: footprints are filtered by their
reference designator and value, zones by the zone name, net and ratsnest items
by the net name, and text (text, textboxes, and dimensions) by the text content.
You can sort the filtered results in ascending or descending order of the value
in a particular column by clicking on that column header.

Filters support wildcards: `*` matches any characters, and `?` matches any
single character. You can also use
http://docs.wxwidgets.org/3.2/overview_resyntax.html[regular expressions], such
as `/footprint value/`.

The displayed information depends on the item type. In addition to the item's
name and/or value, physical items (footprints, zones, and text) list their layer
and X/Y location. Text also displays the type of text object (text, textbox, or
dimension.) Net and ratsnest items list their net name and net class.

When you click an item in the search panel, the item is selected in the editing
canvas. Double-clicking an item in the search panel opens its properties dialog
(for net and ratsnest items, the <<board-setup-net-classes,net classes dialog>>
is opened instead).

Show or hide the search panel with **View** -> **Show Search Panel** or use the
kbd:[Ctrl+G] shortcut.

[[threed-viewer]]
=== 3D Viewer

The 3D Viewer shows a 3-dimensional view of the board and the components on the
board. You can view the board from different perspectives, show or hide
different types of components, cross-probe from the PCB Editor to the 3D viewer,
and generate raytraced renders of the board. Show the 3D Viewer with **View**
-> **3D Viewer** or use the kbd:[Alt+3] shortcut.

image::images/en/3d_viewer.png[3D viewer]

NOTE: The 3D model for a component will only appear if the 3D model file exists
and has been <<working-with-footprints,assigned to the footprint>>.

NOTE: Many footprints in KiCad's standard library do not yet have model files
created for them. However, these footprints may contain a path to a 3D model
that does not yet exist, in anticipation of the 3D model being created in the
future.

==== Navigating the 3D view

Dragging with the left mouse button will orbit the 3D view. By default this is
the centroid of the board, but the pivot point can be reset to a new point on
the board by moving the cursor over the desired point and pressing kbd:[Space].
Scrolling the mouse wheel will zoom the view in or out.  Scrolling while holding
kbd:[Ctrl] pans the view left and right, and scrolling while holding kbd:[Shift]
pans up and down. Dragging with the middle mouse button also pans the view.

Different sized 3D grids can be set using the **View** -> **3D Grid**
menu. Bounding boxes for each component can be enabled with **Preferences** ->
**Show Model Bounding Boxes**.

When the PCB Editor and the 3D Viewer are both open, selecting a footprint in
the PCB Editor will also highlight the component in the 3D Viewer. The highlight
color is adjustable in **Preferences** -> **Preferences...** -> **3D Viewer** ->
**Realtime Renderer** -> **Selection Color**.

==== Appearance manager

The appearance manager is a panel at the right of the viewer which provides
controls to manage the visibility, color, and opacity of different types of
objects and board layers in the 3D view.

Each layer or type of object in the list can be individually shown or hidden by
clicking its corresponding visibility icon. PCB layers can have their colors
customized; double-click on the color swatch next to the item type to edit the
item's color and opacity. To use the colors selected in the Board Setup dialog's
Physical Stackup editor, enable the **use board stackup colors** option.

You can save an appearance configuration as a preset, or load a configuration
from a preset, using the **Preset** selector at the bottom. The kbd:[Ctrl+Tab]
hotkey cycles through presets; press kbd:[Tab] repeatedly while holding
kbd:[Ctrl] to cycle through multiple presets. Several built-in presets are
available: "Follow PCB Editor" matches the visibility settings in
the PCB editor, "Follow PCB Plot Settings" matches the visibility settings
selected in the Plot dialog, and "legacy colors" matches the default 3D Viewer
color settings from older versions of KiCad.

Finally, you can save a viewport for later retrieval using the **Viewports**
selector at the bottom. You can quickly cycle between saved viewports using
kbd:[Shift+Tab]; pressing kbd:[Tab] repeatedly while holding kbd:[Shift] will
cycle through multiple viewports.

==== Generating images with the 3D Viewer

The current 3D view can be saved to an image with **File** -> **Export Current
View as PNG...** or **Export Current View as JPG...**, depending on the desired
image format. The current view can also be copied to the clipboard using the
image:images/icons/copy_24.png[copy icon] button, or **Edit** -> **Copy 3D
Image**.

The 3D Viewer has a raytracing rendering mode which displays the board using a
more physically accurate rendering model than the default rendering mode.
Raytracing is slower than the default rendering mode, but it can be used when
the most visually attractive results are desired. Raytracing mode is enabled
with the image:images/icons/render_mode_24.png[raytracing icon] button, or with
**Preferences** -> **Raytracing**. The 3D grid and selection highlights are not
shown in raytracing mode.

Colors and other rendering options, for both raytraced and non-raytraced modes,
can be adjusted in **Preferences** -> **Preferences...** -> **3D Viewer**.

==== 3D viewer controls

Many viewing options are controlled with the top toolbar.

[width="90%",cols="10%,90%",]
|=======================================================================
|image:images/icons/import3d_24.png[]
|Reload the 3D model

|image:images/icons/copy_24.png[]
|Copy 3D image to clipboard

|image:images/icons/render_mode_24.png[]
|Render current view using raytracing

|image:images/icons/refresh_24.png[]
|Redraw

|image:images/icons/zoom_in_24.png[]
|Zoom in

|image:images/icons/zoom_out_24.png[]
|Zoom out

|image:images/icons/zoom_fit_in_page_24.png[]
|Fit drawing in display area

|image:images/icons/rotate_cw_x_24.png[]
|Rotate X clockwise

|image:images/icons/rotate_ccw_x_24.png[]
|Rotate X counterclockwise

|image:images/icons/rotate_cw_y_24.png[]
|Rotate Y clockwise

|image:images/icons/rotate_ccw_y_24.png[]
|Rotate Y counterclockwise

|image:images/icons/rotate_cw_z_24.png[]
|Rotate Z clockwise

|image:images/icons/rotate_ccw_z_24.png[]
|Rotate Z counterclockwise

|image:images/icons/flip_board_24.png[]
|Flip board view

|image:images/icons/left_24.png[]
|Pan board left

|image:images/icons/right_24.png[]
|Pan board right

|image:images/icons/up_24.png[]
|Pan board up

|image:images/icons/down_24.png[]
|Pan board down

|image:images/icons/ortho.png[]
|Enable/disable orthographic projection

|image:images/icons/layers_manager_24.png[]
|Show/hide the appearance manager
|=======================================================================

[[net-inspector]]
=== Net inspector

The Net Inspector allows you to view statistics about all the nets in a board.  To open the
inspector, click the image:images/icons/list_nets_24.png[] icon at the top of the Nets section of
the Appearance panel, or select **Net Inspector** from the **Inspect** menu.

image::images/net_inspector.png[scaledwidth="70%"]

Clicking a net in the list will highlight that net on the board.  Clicking a column title allows
you to sort the list of nets by that column.

The **Group By** field allows you to combine different nets together and view the total length of the
combined nets.  For example, if you have two nets named `DATA0` and `DATA0_EXT`, using a Group By
value of `DATA0*` will create a group containing both nets.  More complicated groupings can be
created by changing the Group By mode from Wildcard to RegEx (regular expressions).  The substring
(Substr) variants of the Group By mode will create groups for each set of nets that matches the
pattern differently.

For example, if you have the nets `U1D+`, `U1D-`, `U2D+`, and `U2D-`, the grouping pattern `U*D`
will match all four nets in Wildcard mode, creating a single group `U*D`.  In Wildcard Substr mode,
it will match all four nets, but create two different groups: `U1D` and `U2D`.

**Pad Count** and **Via Count** show the number of pads (surface mount and through hole) and vias
on a net.  **Via Length** shows the total height of each via (not accounting for which copper
layers the via connects to).  In other words, Via Length is equal to Via Count multiplied by the
stackup height of the board.  **Track Length** shows the total length of all track segments in a
net, not accounting for topology.  **Die length** shows the total of all Pad to Die Length values
set for pads on the net.

==== Differences between Net Inspector and Length Tuner

The Net Inspector may report different net lengths than the <<length-tuning,length tuner>>, because
the two tools have different purposes and calculate track/net lengths differently. In short, the
Net Inspector sums up the total length of each track segment and via on a net, while the length tuner
calculates the effective electrical length of a path between two points on a net. The specific
differences are as follows:

- The Net Inspector reports track length as a simple sum of the length of each track segment on a net.
  The length tuner calculates an effective electrical length of a net, which includes optimizing paths
  through pads to calculate the shortest possible path.
- If a routed net has a branching topology, the Net Inspector total includes the length of each branch
  in the total. The length tuner calculates a point-to-point length; if there are any branches, the
  length tuner will stop at the closest branch and report the length up to the branch.
- The Net Inspector always includes the effective via height in its via length and total length
  calculations. If a via connects to traces on both the top and bottom layers, the full via height is
  included in the length calculation. Otherwise, only the stackup height between the connected layers
  is included. The length tuner calculates effective via height in the same way as the Net Inspector,
  but via height is only included in the length calculation when the **use stackup height** setting is
  enabled <<board-setup-constraints,board constraint settings>>. If the setting is disabled, the
  length tuner will not include vias in its calculations at all.